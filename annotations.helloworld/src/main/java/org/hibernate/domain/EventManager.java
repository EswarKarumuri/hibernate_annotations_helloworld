package org.hibernate.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class EventManager {

	public static void main(String[] args) {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Person person = new Person();
		person.setFirstname("Bujji");
		person.setLastname("Bujjamma");
		person.setAge(28);
		Long id = (Long)session.save(person);
		session.getTransaction().commit();
		System.out.println(id);
	}

}
